﻿using ChatApi.RPC;

namespace ChatApi.Middleware
{
    public class TokenAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;

        public TokenAuthorizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var authorizationHeader = context.Request.Headers["Authorization"].FirstOrDefault();

            if (string.IsNullOrEmpty(authorizationHeader))
            {
                context.Response.StatusCode = 401; // Unauthorized
                return;
            }

            var token = authorizationHeader.Replace("Bearer ", string.Empty);

            if (!await CheckAuthorize(token))
            {
                context.Response.StatusCode = 403; // Forbidden
                return;
            }

            await _next(context);
        }

        private async Task<bool> CheckAuthorize(string token)
        {
            using var rpcClient = new RpcClient();

            Console.WriteLine($"Requesting {token}");
            var response = await rpcClient.CallAsync(token);
            Console.WriteLine($"Response {response}");

            return bool.Parse(response);
        }
    }
}
