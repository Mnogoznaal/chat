﻿using Microsoft.AspNetCore.SignalR;

namespace ChatApi.SignalRHub
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string message)
        {
            await Clients.All.SendAsync("newMessage", message);
        }
    }
}
