﻿using ChatApi.SignalRHub;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using StackExchange.Redis;

namespace ChatApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IDatabase _db;
        private readonly IHubContext<ChatHub> _hubContext;
        private const string ChatKey = "chat_messages";

        public ChatController(IHubContext<ChatHub> hubContext, IDatabase db)
        {
            _hubContext = hubContext;
            _db = db;
        }
        [HttpGet]
        public ActionResult GetMessages()
        {
            var messages = _db.ListRange(ChatKey).Select(redisValue => (string)redisValue).ToList();
            return Ok(messages);
        }

        [HttpPost]
        public ActionResult<string> SendMessage([FromBody] string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                return BadRequest("Message cannot be empty.");
            }

            _db.ListLeftPush(ChatKey, message);

            _hubContext.Clients.All.SendAsync("newMessage", message);

            return Ok(message);
        }
    }
}
